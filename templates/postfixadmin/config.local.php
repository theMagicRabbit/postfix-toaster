<?php
$CONF['database_type'] = 'mysqli';
$CONF['database_host'] = 'localhost';
$CONF['database_user'] = '{{ db_user }}';
$CONF['database_password'] = '{{ db_user_pw }}';
$CONF['database_name'] = 'postfixadmin';

$CONF['default_aliases'] = array (
  'abuse'       => 'abuse@themagicrabbit.xyz',
  'hostmaster'  => 'hostmaster@themagicrabbit.xyz',
  'postmaster'  => 'postmaster@themagicrabbit.xyz',
  'webmaster'   => 'webmaster@themagicrabbit.xyz'
);

$CONF['fetchmail'] = 'NO';
$CONF['show_footer_text'] = 'NO';

$CONF['quota'] = 'YES';
$CONF['domain_quota'] = 'YES';
$CONF['quota_multiplier'] = '1024000';
$CONF['used_quotas'] = 'YES';
$CONF['new_quota_table'] = 'YES';

$CONF['aliases'] = '0';
$CONF['mailboxes'] = '0';
$CONF['maxquota'] = '0';
$CONF['domain_quota_default'] = '0';

$CONF['configured'] = true;
?>
